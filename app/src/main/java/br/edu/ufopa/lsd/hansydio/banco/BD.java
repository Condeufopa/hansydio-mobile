package br.edu.ufopa.lsd.hansydio.banco;

import android.provider.BaseColumns;

public final class BD {

    public static final String NOME = "hansydio.db";
    public static final int VERSAO = 1;

    public BD() {}

    public static abstract class Detentos implements BaseColumns {
        public static final String NOME_TABELA                           = "detentos";
        public static final String NUMERO                                = "numero";
        public static final String ANO                                   = "ano";
        public static final String CAMPANHA                              = "campanha";
        public static final String DATA_CAMPANHA                         = "data_campanha";
        public static final String NOME                                  = "nome";
        public static final String SEXO                                  = "sexo";
        public static final String NASCIMENTO                            = "nascimento";
        public static final String IDADE                                 = "idade";
        public static final String ESTADO_CIVIL                          = "estado_civil";
        public static final String NATURALIDADE                          = "naturalidade";
        public static final String MUNICIPIO_ULTIMA_RESIDENCIA           = "municipio_ultima_residencia";
        public static final String GRAU_INSTRUCAO                        = "grau_instrucao";
        public static final String ULTIMA_PROFISSAO                      = "ultima_profissao";
        public static final String TEMPO_PRIVACAO_LIBERDADE              = "tempo_privacao_liberdade";
        public static final String FICOU_FUNDACAO_CASA_IDADE             = "ficou_fundacao_casa_idade";
        public static final String FICOU_FUNDACAO_CASA_TEMPO             = "ficou_fundacao_casa_tempo";
        public static final String PAROU_DROGAS_QUANDO                   = "parou_drogas_quando";
        public static final String IDADE_EXPERIMENTOU_DROGA_PRIMEIRA_VEZ = "idade_experimentou_droga_primeira_vez";
        public static final String DROGA_EXPERIMENTOU_PRIMEIRA_VEZ       = "droga_experimentou_primeira_vez";
        public static final String OBSERVACOES                           = "observacoes";
        public static final String RESPONSAVEL                           = "responsavel";
        public static final String CRIADO                                = "criado";
        public static final String ATUALIZADO                            = "atualizado";
    }

    public static abstract class Penitenciarias implements BaseColumns {
        public static final String NOME_TABELA         = "penitenciarias";
        public static final String NUMERO_DETENTO      = "numero_detento";
        public static final String NOME                = "nome";
        public static final String REGIME              = "regime";
        public static final String TEMPO               = "tempo";
        public static final String CELA                = "cela";
        public static final String NUMERO_PESSOAS_CELA = "numero_pessoas_cela";
    }

    public static abstract class ExameMedicoFeminino implements BaseColumns {
        public static final String NOME_TABELA                 = "exame_medico_feminino";
        public static final String NUMERO_DETENTO              = "numero_detento";
        public static final String ULTIMA_MENSTRUACAO          = "ultima_menstruacao";
        public static final String MENSTRUACAO_OPCAO           = "menstruacao_opcao";
        public static final String ESTA_GRAVIDA                = "esta_gravida";
        public static final String MESES_GRAVIDA               = "meses_gravida";
        public static final String JA_ENGRAVIDOU               = "ja_engravidou";
        public static final String QUANTIDADE_GESTACOES        = "quantidade_gestacoes";
        public static final String QUANTIDADE_PARTOS_NORMAIS   = "quantidade_partos_normais";
        public static final String QUANTIDADE_CESARIAS         = "quantidade_cesarias";
        public static final String QUANTIDADE_FILHOS_VIVOS     = "quantidade_filhos_vivos";
        public static final String SOFREU_ABORTO               = "sofreu_aborto";
        public static final String QUANTIDADE_ABORTOS          = "quantidade_abortos";
        public static final String SOFREU_VIOLENCIA_SEXUAL     = "sofreu_violencia_sexual";
        public static final String QUANTIDADE_VIOLENCIA_SEXUAL = "quantidade_violencia_sexual";
        public static final String DENUNCIOU_AGRESSORES        = "denunciou_agressores";
        public static final String MOTIVO_DENUNCIA_AGRESSORES  = "motivo_denuncia_agressores";
    }

    public static abstract class LocaisAfetados implements BaseColumns {
        public static final String NOME_TABELA    = "locais_afetados";
        public static final String NUMERO_DETENTO = "numero_detento";
        public static final String LOCAL          = "local";
        public static final String TIPO           = "tipo";
    }

    public static abstract class ExameMedicoHanseniase implements BaseColumns {
        public static final String NOME_TABELA               = "exame_medico_hanseniase";
        public static final String NUMERO_DETENTO            = "numero_detento";
        public static final String NUMERO_LESOES             = "numero_lesoes";
        public static final String CICATRIZ_BCG              = "cicatriz_bcg";
        public static final String SENSIBILIDADE_TESTADA     = "sensibilidade_testada";
        public static final String TESTE_HISTAMINA_ENDOGENA  = "teste_histamina_endogena";
        public static final String TESTE_ALIZARINA           = "teste_alizarina";
        public static final String CASO_CONFIRMADO           = "caso_confirmado";
        public static final String CLASSIFICACAO_OPERACIONAL = "classificacao_operacional";
        public static final String REACAO_HANSENICA          = "reacao_hansenica";
        public static final String CLASSIFICACAO             = "classificacao";
    }

    public static abstract class NervosAcometidos implements BaseColumns {
        public static final String NOME_TABELA    = "nervos_acometidos";
        public static final String NUMERO_DETENTO = "numero_detento";
        public static final String NERVO          = "nervo";
    }

    public static abstract class GruposLesao implements BaseColumns {
        public static final String NOME_TABELA               = "grupos_lesao";
        public static final String NUMERO_DETENTO            = "numero_detento";
        public static final String SOMENTE_AREA_HIPOESTESICA = "somente_area_hipoestesica";
        public static final String MACULA_HIPOCROMICA 		 = "macula_hipocromica";
        public static final String TUBERCULO 				 = "tuberculo";
        public static final String FOVEA 					 = "fovea";
        public static final String NODULO 					 = "nodulo";
        public static final String INFILTRACAO 				 = "infiltracao";
        public static final String PLACA 					 = "placa";
        public static final String OBSERVACAO                = "observacao";
    }

    public static abstract class LocaisLesao implements BaseColumns {
        public static final String NOME_TABELA = "locais_lesao";
        public static final String ID_GRUPO    = "id_grupo";
        public static final String LOCAL 	   = "local";
        public static final String TAMANHO 	   = "tamanho";
    }

    public static abstract class DoencasContraidas implements BaseColumns {
        public static final String NOME_TABELA        = "doencas_contraidas";
        public static final String NUMERO_DETENTO     = "numero_detento";
        public static final String DOENCA             = "doenca";
        public static final String POSSUI             = "possui";
        public static final String TRATOU_OU_TRATANDO = "tratou_ou_tratando";
        public static final String TIPO               = "tipo";
    }

    public static abstract class DrogasUsadas implements BaseColumns {
        public static final String NOME_TABELA    = "drogas_usadas";
        public static final String NUMERO_DETENTO = "numero_detento";
        public static final String DROGA          = "droga";
    }

}